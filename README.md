# outToSomeFile -- Utility to redirect output of a executable to an appropriate filename.


## When useful?
Useful for dumping the results of runs of some configurable executable program,
when the executable takes command line options and writes output to stdout.

Saves the user the effort of thinking of and typing in filenames for each run.


## Example
`% outToFile -d runs ./myCommand -s 0.23`

Runs ./myCommand -s 0.23
Redirecting its output to a file with a name like:  
runs/20171204110656myCommand__-s__0.23

Where in this example the program was run at 11:06:56 on December 4, 2017.

outToFile also creates a similarly named file recording running time etc via the "time" command.


`% outToFile ./myCommand ~/inputFiles/input1.fn`

Runs ./myCommand
Redirecting to an output file with a name like:  
20221106095521myCommand__／inputFiles／input1.fn

Note that replacement of '/' with '／' here to disguise the pathname nature of the string when
intended only to be part of a regular filename.

### -u flag
The -u flag is convenient when the output files are stored in a subdirectory,
and your shell happens to already be in that directory.

Assuming we start in directory .../runs/

`% outToFile -u -d runs ./myCommand -s 0.74`

Is a shortcut for

`% cd ..; outToFile -d runs ./myCommand -s 0.74; cd runs`


## Copyright
Paul Horton, 2017,2018,2019,2022
